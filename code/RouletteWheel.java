import java.util.Random;
public class RouletteWheel
{
    private int number=0;
   
    private Random rand; 

    public RouletteWheel(){
       rand = new Random();
    }

    public int getValue(){
        return number;
    }

    public void spin()
    {
       
        number=rand.nextInt(37);
    }
}