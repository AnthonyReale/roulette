import java.util.Scanner;
public class Roulette
{
    public static void main(String[]args){
        Scanner scan = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        int bank=1000;
        System.out.println("would you like to make a bet, 1 for yes, 2 for no");
        int play=scan.nextInt();

        while(play==1)
        {
            System.out.println("enter betting amount");
            int bet=scan.nextInt();
            while(bet<=bank && bet>0)
            {
                bank-=bet;
                wheel.spin();
                System.out.println("1 for number| 2 for even| 3 for odd| 4 for Black| 5 for Red ");
                int result= scan.nextInt();
                if(result==1)
                {
                    System.out.println("Enter Number from 0-35");
                    int number = scan.nextInt();
                    isNumber(number,bet,bank,wheel);
                    System.out.println("remaining currency $"+bank);
                }
                else if(result==2)
                {
                    bank=ifEven(bet,bank,wheel);
                    System.out.println("remaining currency $"+bank);
                }
                else if(result==3)
                {
                    bank=ifOdd(bet,bank,wheel);
                    System.out.println("remaining currency $"+bank);
                }
                else if(result==4)
                {
                    bank=ifBlack(bet,bank,wheel);
                    System.out.println("remaining currency $"+bank);
                }
                else
                {
                    bank=ifRed(bet,bank,wheel);
                    System.out.println("remaining currency $"+bank);
                }

                System.out.println("would you like to place a bet, 1=yes,2=No");
                int replay=scan.nextInt();
                if(replay==1)
                {
                    System.out.println("enter betting amount");
                    bet=scan.nextInt();
                }
                else
                {
                    break;
                }
            }

            System.out.println("your leaving with a total of $"+bank);
            break;
        }
    }

    public static int ifEven(int bet,int bank,RouletteWheel wheel)
    {
        if(wheel.getValue()%2==0 && wheel.getValue()!=0)
        {
            bet=bet*2;
            bank=bank+bet;
            System.out.println("landed on "+wheel.getValue()+" even, you won $"+ bet);
            return bank;
        }
        else
        {
            System.out.println("landed on "+wheel.getValue()+" odd, you lost $"+bet);
            return bank;
        }
    }
     public static int ifOdd(int bet,int bank,RouletteWheel wheel)
    {
        if(wheel.getValue()%2!=0 || wheel.getValue()==0)
        {
            bet=bet*2;
            bank+=bet;
            System.out.println("landed on "+wheel.getValue()+" odd, you won $"+ bet);
            return bank;
        }
        else
        {
            System.out.println("landed on "+wheel.getValue()+" even, you lost $"+bet);
            return bank;
        }
    }

    public static int ifBlack(int bet,int amount,RouletteWheel wheel)
    {
        if(wheel.getValue()%2==0 && wheel.getValue()!=0)
        {
            bet=bet*2;
             System.out.println("landed on "+wheel.getValue()+" black,you won $"+ bet);
            amount+=bet;
            return amount;
        }
        else
        {
            System.out.println("landed on "+wheel.getValue()+" red,you lost $"+bet);
            return amount;
        }
    }

    public static int ifRed(int bet,int amount,RouletteWheel wheel)
    {
        if(wheel.getValue()%2!=0 && wheel.getValue()!=0 )
        {
            bet=bet*2;
            System.out.println("landed on "+wheel.getValue()+" red,you won $"+ bet);
            amount+=bet;
            return amount;
        }
        else
        {
            System.out.println("landed on "+wheel.getValue()+" black,you lost $"+bet);
            return amount;
        }
    }

    public static int isNumber (int x, int bet, int bank,RouletteWheel wheel)
    {
      if(wheel.getValue()==x)
      {
        bet=bet*35;
        System.out.println("the ball landed on "+wheel.getValue()+" and you have won $"+bet);
        bank+=bet;
        return bank;
      }  
      else
      {
        System.out.println("the ball landed on "+wheel.getValue()+" and you have lost $"+bet);
        return bank;
      }
    }
}